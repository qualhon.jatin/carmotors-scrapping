<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("button").click(function(){
    $("p").hide();
  });
});
</script>
</head>
<body>
<?php 
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    ini_set('memory_limit', '-1'); // unlimited memory limit
    ini_set('max_execution_time', 3000);

    $servername = "localhost";
    $username = "root";
    $password = "12345678";
    $dbname = "autotrader";
    $conn = new mysqli($servername, $username, $password, $dbname) or die('cound not connect db');

    include 'simple_html_dom.php';
    $parms = array(
        'sort' =>	"relevance",
        'postcode' =>	"wc2n5du",
        'radius' =>	"35",
        'make' =>	"LAND ROVER",
        'search-results-price-type' =>	"total-price",
        'search-results-year' => "select-year",
        'newCarHasDeal' =>	"on",
        'ulez-compliant' =>	"on",
        'writeoff-categories' =>	"on",
        'page' => @$_GET['page']?$_GET['page']:'1'
    );
    $data = http_build_query($parms);
    $url = "https://www.autotrader.co.uk/results-car-search";
    $getUrl = $url."?".$data;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, $getUrl);
    $response = curl_exec($ch); 
    if(curl_error($ch)){
        echo 'Request Error:' . curl_error($ch);
    }
    else
    {
        $response = json_decode($response,true);
        decodeHTML($response['html']);
    }    
    curl_close($ch);

    function decodeHTML($html) {
        //echo '<div style="display:block">'.$html.'</div>';
        $html = str_get_html($html);        
        foreach ($html->find('ul.search-page__results') as $element) {            
            foreach($element->find('li') as $li) {
                $setData = array();
                foreach($li->find('figure[class="listing-main-image"]') as $figure) {
                    foreach($figure->find('a') as $anchor) {
                        foreach($anchor->find('img') as $img) {
                            //echo $img->src; 
                            $setData['image'] = trim($img->src);                         
                        }
                    }
                }
                foreach($li->find('div[class="information-container"]') as $info) {
                    foreach($info->find('h2') as $h2) {
                        foreach ($h2->find('a') as $anchor) {
                            //echo $anchor->innertext;
                            $setData['title'] = trim($anchor->innertext);
                        }
                    }
                    foreach ($info->find('ul[class="listing-key-specs"]') as $ul) {
                        //echo $ul->innertext;
                        $setData['detail'] = strip_tags($ul->innertext);
                        $setData['detail'] = preg_replace('/\s+/', ' ', strip_tags($ul->innertext));
                    }
                }
                foreach ($li->find('.price-column') as $price) {
                    foreach ($price->find('a') as $anchor) {
                        foreach ($anchor->find('.physical-stock-now') as $stock) {
                            foreach ($stock->find('.price-red') as $currentPrice) {
                                //echo $currentPrice->innertext;
                                $setData['price'] = trim($currentPrice->innertext);
                            }
                        }
                    }
                }               
                if(!empty($setData)) {
                    global $parms;
                    $qry = "INSERT INTO `scrap_data`(`parms`,`data`) VALUES ('".json_encode($parms)."','".json_encode($setData)."')";
                    global $conn;
                    mysqli_query($conn,$qry);                    
                }

            }            
        }
        echo 'Done.....!';
    }

?>
</body>
</html