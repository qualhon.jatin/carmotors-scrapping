<?php

    print_r($_GET);

    $make = $_GET['make'] ? $_GET['make'] : "";
    $model = $_GET['model'] ? $_GET['model'] : "";
    $page = $_GET['page'] ? $_GET['page'] : "";

    if(empty($make) || empty($model) || empty($page)){
        die("Something went wrong");
    }

    $url = "https://www.motors.co.uk/search/car/results";
    $body = '{"isNewSearch":true,"pagination":{"TotalPages":0,"BasicResultCount":3610,"TotalRecords":3610,"FirstRecord":1,"LastRecord":15,"CurrentPage":'.$page.',"LastPage":241,"PageSize":500,"PageLinksPerPage":5,"PageLinks":[{"Name":"1","Link":"1"},{"Name":"2","Link":"2"},{"Name":"3","Link":"3"},{"Name":"4","Link":"4"},{"Name":"5","Link":"5"}],"FirstPageLink":{"Name":"1","Link":"1"},"Level":null,"Variants":0,"CurrentPageLvl1":1,"CurrentPageLvl2":1,"CurrentPageLvl3":1,"CurrentPageLvl4":1},"searchPanelParameters":{"Doors":[],"Seats":[],"SafetyRatings":[],"SelectedTopSpeed":null,"SelectedPower":null,"SelectedAcceleration":null,"MinPower":-1,"MaxPower":-1,"MinEngineSize":-1,"MaxEngineSize":-1,"BodyStyles":[],"DriveTrains":[],"MakeModels":[{"Value":"'.$make.'","Models":["'.$model.'"],"Trims":[]}],"FuelTypes":[],"Transmissions":[],"Colours":[],"IsPaymentSearch":false,"IsReduced":false,"IsHot":false,"IsRecentlyAdded":false,"IsRecommendedSearch":true,"VoucherEnabled":false,"IsGroupStock":false,"PartExAvailable":false,"IsPriceAndGo":false,"IsPriceExcludeVATSearch":false,"IncludeOnlineOnlySearch":true,"IsYearSearch":false,"IsPreReg":false,"IsExDemo":false,"ExcludeExFleet":false,"ExcludeExHire":false,"Keywords":[],"SelectedInsuranceGroup":null,"SelectedFuelEfficiency":null,"SelectedCostAnnualTax":null,"SelectedCO2Emission":null,"SelectedTowingBrakedMax":null,"SelectedTowingUnbrakedMax":null,"SelectedAdvertType":"*","SelectedTankRange":null,"DealerId":0,"Age":-1,"MinAge":-1,"MaxAge":-1,"MinYear":-1,"MaxYear":-1,"Mileage":-1,"MinMileage":-1,"MaxMileage":-1,"MinPrice":-1,"MaxPrice":-1,"MinPaymentMonthlyCost":-1,"MaxPaymentMonthlyCost":-1,"PaymentTerm":60,"PaymentMileage":10000,"PaymentDeposit":1000,"SelectedSoldStatus":"both","SelectedBatteryRangeMiles":null,"SelectedBatteryFastChargeMinutes":null,"BatteryIsLeased":false,"BatteryIsWarrantyWhenNew":false,"ExcludeImports":false,"ExcludeHistoryCatNCatD":false,"ExcludeHistoryCatSCatC":false,"ExcludedVehicles":[],"Type":1,"PostCode":"W1A0AX","Distance":1000,"PaginationCurrentPage":1,"SortOrder":0,"DealerGroupId":0,"MinImageCountActive":false}}';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type:application/json')); 
    $output = curl_exec($ch);
    

    if (!curl_exec($ch)) {
        die('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));
    }
    curl_close($ch);
    echo "<pre>";
    // print_r($output);
    $result = (json_decode($output));
    $result = json_decode(json_encode($result),true);
    
    

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "carmotors";

    $conn = new mysqli($servername, $username, $password, $dbname);
    // echo "</pre";

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    $count = 0;
    foreach($result['Results'] as $res){
    
        if($res['ObjectType'] == "UsedVehicleResult"){
            $data = json_encode($res);
            $sql = "INSERT INTO cars (post_code, car_make, car_model, data)
            VALUES ('W1A0AX', '".$make."', '".$model."','".$data."')";

            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully <br>";
            } else {
                echo "Line no ".$count;
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
        $count++;
    }
    
    
    echo "Total Record : " .$count;
    echo "<br>";
    print_r($result['AdTagsDataOutput']);
    print_r($result['Pagination']);
    //print_r($result['Results']);

?>